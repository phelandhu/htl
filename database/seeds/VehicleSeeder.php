<?php

use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicles')->insert([
            'year' => 2020,
            'make' => Str::random(10),
            'model' => Str::random(10),
            'vin' => Str::random(17),
        ]);
        DB::table('vehicles')->insert([
            'year' => 2020,
            'make' => Str::random(10),
            'model' => Str::random(10),
            'vin' => Str::random(17),
        ]);
        DB::table('vehicles')->insert([
            'year' => 2020,
            'make' => Str::random(10),
            'model' => Str::random(10),
            'vin' => Str::random(17),
        ]);
        DB::table('vehicles')->insert([
            'year' => 2020,
            'make' => Str::random(10),
            'model' => Str::random(10),
            'vin' => Str::random(17),
        ]);
    }
}
