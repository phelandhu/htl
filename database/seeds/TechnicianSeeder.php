<?php

use Illuminate\Database\Seeder;

class TechnicianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technicians')->insert([
            'name_first' => 'Jacob',
            'name_last' => 'Marley',
            'truck_number' => 2,
        ]);
        DB::table('technicians')->insert([
            'name_first' => 'Bob',
            'name_last' => 'Smith',
            'truck_number' => 5,
        ]);
        DB::table('technicians')->insert([
            'name_first' => 'Blues',
            'name_last' => 'guitar',
            'truck_number' => 7,
        ]);
        DB::table('technicians')->insert([
            'name_first' => 'No',
            'name_last' => 'Jazz',
            'truck_number' => 1,
        ]);
    }
}
