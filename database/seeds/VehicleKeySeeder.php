<?php

use Illuminate\Database\Seeder;

class VehicleKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicles_keys')->insert([
            'vehicle_id' => 1,
            'key_id' => 3,
        ]);
        DB::table('vehicles_keys')->insert([
            'vehicle_id' => 2,
            'key_id' => 2,
        ]);
        DB::table('vehicles_keys')->insert([
            'vehicle_id' => 3,
            'key_id' => 4,
        ]);
        DB::table('vehicles_keys')->insert([
            'vehicle_id' => 4,
            'key_id' => 1,
        ]);
    }
}
