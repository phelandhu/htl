<?php

use Illuminate\Database\Seeder;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keys')->insert([
            'name' => 'Key X',
            'description' => 'A Key of X',
            'price' => 175.50,
        ]);
        DB::table('keys')->insert([
            'name' => 'Key Jazz',
            'description' => 'A Jazzy Key',
            'price' => 150.75,
        ]);
        DB::table('keys')->insert([
            'name' => 'Key Y',
            'description' => 'Y ask Y',
            'price' => 22.00,
        ]);
        DB::table('keys')->insert([
            'name' => 'Key Z',
            'description' => 'That\'s Key Zed for Canadians',
            'price' => 79.99,
        ]);
    }
}
