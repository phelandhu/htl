<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'key_id' => 4,
            'technician_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('orders')->insert([
            'key_id' => 3,
            'technician_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('orders')->insert([
            'key_id' => 1,
            'technician_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('orders')->insert([
            'key_id' => 2,
            'technician_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
