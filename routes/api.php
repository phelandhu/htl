<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('keys', 'KeyController@index');
Route::get('keys/{id}', 'KeyController@show');

Route::get('vehicles', 'VehicleController@index');
Route::get('vehicles/{id}', 'VehicleController@show');

Route::get('technicians', 'TechnicianController@index');
Route::get('technicians/{id}', 'TechnicianController@show');

Route::get('orders', 'OrderController@index');
//Route::get('orders/{id}', 'OrderController@show');
Route::post('orders', 'OrderController@store');
Route::put('orders/{id}', 'OrderController@update');
Route::delete('orders/{id}', 'OrderController@delete');


Route::get('orders/keys', 'OrderController@showByKey');

Route::get('orders/technicians', 'OrderController@showByTechnician');

Route::get('orders/vehicles', 'OrderController@showByVehicle');
