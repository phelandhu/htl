<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['name' => 'James']);
});

Route::get('/orders/', 'OrderListViewController@show');

Route::get('/orders/{id}', 'OrderViewController@show');

Route::get('/ordersNew', 'OrderNewViewController@show');

Route::post('/ordersNew', 'OrderNewViewController@store');

