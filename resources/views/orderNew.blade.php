<!DOCTYPE html>
<html>
    <body>
        <br />
        <form method="POST" action="/ordersNew">
            @csrf
            <h2>Key Selection</h2>
            <select class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="dropdown">
                @foreach($keys as $key)
                    <option value="{{ $key->id }}">{{$key->name}} - {{$key->description}}</option>
                @endforeach
            </select>
            <br />
            <h2>Technician Selection</h2>
            <select class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="dropdown">
                @foreach($technicians as $technician)
                    <option value="{{ $technician->id }}">{{$technician->name_last}}, {{$technician->name_first}}</option>
                @endforeach
            </select>
            <br />
            <input type="submit" value="Submit">
        </form>
    </body>
</html>