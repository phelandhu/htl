<!DOCTYPE html>
<html>
<body>
<h1>Orders</h1>
@foreach ($orders as $order)
    <b>Key name:</b> {{$order->name}}<br />
    <b>Key Description:</b> {{$order->description}}<br />
    <b>Key Price:</b> {{$order->price}}<br />
    <b>Vehicle Year:</b> {{$order->year}}<br />
    <b>Vehicle Make:</b> {{$order->make}}<br />
    <b>Vehicle Model:</b> {{$order->model}}<br />
    <b>Vehicle VIN:</b> {{$order->vin}}<br />
    <b>Technician Last Name:</b> {{$order->name_last}}<br />
    <b>Technician First Name:</b> {{$order->name_first}}<br />
    <b>Truck Number:</b> {{$order->truck_number}}<br />
    <br />
@endforeach
</body>
</html>