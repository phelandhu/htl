<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderViewController extends Controller
{
    public function show(Request $request, $id)
    {
        $orders = app('App\Http\Controllers\OrderController')->showOrderByKey($id);
        return view('orderView', ['orders' => $orders]);
    }
}
