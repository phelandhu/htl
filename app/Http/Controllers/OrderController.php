<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Http\Requests\StoreOrderRequest;

class OrderController extends Controller
{

    public function index()
    {
        return Order::all();
    }

    public function show($id)
    {
        return Order::find($id);
    }

    public function store(StoreOrderRequest $request)
    {
        return Order::create($request->all());
    }

    public function update(StoreOrderRequest $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->update($request->all());

        return $order;
    }

    public function delete(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return 204;
    }


    public function showByKey()
    {
        $orders = DB::table('orders')
            ->join('keys', 'keys.id', '=', 'orders.key_id')
            ->join('vehicles_keys', 'keys.id', '=', 'vehicles_keys.key_id')
            ->join('vehicles', 'vehicles.id', '=', 'vehicles_keys.vehicle_id')
            ->join('technicians', 'technicians.id', '=', 'orders.technician_id')
            ->select('orders.*', 'technicians.name_last', 'technicians.name_first', 'technicians.truck_number', 'keys.name', 'keys.description', 'keys.price', 'vehicles.id as vehicle_id', 'vehicles.year', 'vehicles.make', 'vehicles.model', 'vehicles.vin')
            ->orderByDesc('keys.id')
            ->get();
        return $orders;
    }

    public function showOrderByKey($id)
    {
        $orders = DB::table('orders')
            ->join('keys', 'keys.id', '=', 'orders.key_id')
            ->join('vehicles_keys', 'keys.id', '=', 'vehicles_keys.key_id')
            ->join('vehicles', 'vehicles.id', '=', 'vehicles_keys.vehicle_id')
            ->join('technicians', 'technicians.id', '=', 'orders.technician_id')
            ->select('orders.*', 'technicians.name_last', 'technicians.name_first', 'technicians.truck_number', 'keys.name', 'keys.description', 'keys.price', 'vehicles.id as vehicle_id', 'vehicles.year', 'vehicles.make', 'vehicles.model', 'vehicles.vin')
            ->where('orders.id', '=', $id)
            ->get();
        return $orders;
    }

    public function showByTechnician()
    {
        $orders = DB::table('orders')
            ->join('keys', 'keys.id', '=', 'orders.key_id')
            ->join('vehicles_keys', 'keys.id', '=', 'vehicles_keys.key_id')
            ->join('vehicles', 'vehicles.id', '=', 'vehicles_keys.vehicle_id')
            ->join('technicians', 'technicians.id', '=', 'orders.technician_id')
            ->select('orders.*', 'technicians.name_last', 'technicians.name_first', 'technicians.truck_number', 'keys.name', 'keys.description', 'keys.price', 'vehicles.id as vehicle_id', 'vehicles.year', 'vehicles.make', 'vehicles.model', 'vehicles.vin')
            ->orderByDesc('orders.technician_id')
            ->get();
        return $orders;
    }

    public function showByVehicle()
    {
        $orders = DB::table('orders')
            ->join('keys', 'keys.id', '=', 'orders.key_id')
            ->join('vehicles_keys', 'keys.id', '=', 'vehicles_keys.key_id')
            ->join('vehicles', 'vehicles.id', '=', 'vehicles_keys.vehicle_id')
            ->join('technicians', 'technicians.id', '=', 'orders.technician_id')
            ->select('orders.*', 'technicians.name_last', 'technicians.name_first', 'technicians.truck_number', 'keys.name', 'keys.description', 'keys.price', 'vehicles.id as vehicle_id', 'vehicles.year', 'vehicles.make', 'vehicles.model', 'vehicles.vin')
            ->orderByDesc('vehicles.id')
            ->get();
        return $orders;
    }

}
