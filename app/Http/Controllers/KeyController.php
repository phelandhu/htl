<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Key;

class KeyController extends Controller
{
    public function index()
    {
        return Key::all();
    }

    public function show($id)
    {
        return Key::find($id);
    }
}
