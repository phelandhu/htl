<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreOrderRequest;

class OrderNewViewController extends Controller
{
    public function show()
    {
        $keys = app('App\Http\Controllers\KeyController')->index();
        $technicians = app('App\Http\Controllers\TechnicianController')->index();
        return view('orderNew', ['keys' => $keys, 'technicians' => $technicians]);
    }

    public function store(StoreOrderRequest $request) {
        app('App\Http\Controllers\OrderController')->store($request);
        return redirect()->route('/orders/');
    }
}
