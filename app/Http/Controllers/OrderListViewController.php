<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class OrderListViewController extends Controller
{
    public function show()
    {
        $orders = app('App\Http\Controllers\OrderController')->showByKey();
        return view('orderList', ['orders' => $orders]);
    }
}
