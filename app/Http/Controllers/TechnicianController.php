<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Technician;

class TechnicianController extends Controller
{
    public function index()
    {
        return Technician::all();
    }

    public function show($id)
    {
        return Technician::find($id);
    }
}
