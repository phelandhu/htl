<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key_id' => 'required',
            'technician_id' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'key_id.required'     => 'A key ID for the Order is required.',
            'technician_id.required'  => 'A Technician ID of the Order is required.',
        ];
    }
}
