<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    protected $table = 'keys';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'description', 'price'];

    public function vehicle() {
        return $this->hasOne('App\Vehicle');
    }
}
