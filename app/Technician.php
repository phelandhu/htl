<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $table = 'technicians';
    protected $primaryKey = 'id';
    protected $fillable = ['name_first', 'name_last', 'truck_number'];
}
